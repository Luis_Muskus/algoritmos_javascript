// Generar todas las tabla de multiplicar de numero en pantalla

function table_mult(table) {
    
    console.log(`\nTabla del ${table}`);
    for (let multiplicator = 0; multiplicator < 11; multiplicator++) {
    
      console.log(`${table} X ${multiplicator} = ${table * multiplicator}`);
    
  }
  return true;

}

init= table_mult(4);
console.log(init);

module.exports = {
    table_mult,
  }