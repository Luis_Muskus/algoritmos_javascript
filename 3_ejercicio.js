// Leer un número entena de dos digitos y determinar si ambos digitos son pares.
//const numero = 11;

function par_digits(num) {
      
    if (num >= 10 && num <= 99) {
      let digit1 = parseInt(num / 10);
      let digit2 = parseInt(num % 10);
      if (digit1 % 2 == 0 && digit2 % 2 == 0) {
        console.log('Los dos digitos son pares');
        return true;
      }else if (digit1 % 2 == 0){
        console.log('Solo digito1 es par');
      }else if (digit2 % 2 == 0){
        console.log('Solo digito2 es par');
      }else{
        console.log('son impares');
        return false;
      }
    }

}

init= par_digits(22);
console.log(init);

module.exports = {
    par_digits
  }

