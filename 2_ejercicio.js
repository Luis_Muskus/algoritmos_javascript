// Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos.

function two_digits(num){

    if (num >= 10 && num <= 99) {

      //return true;
      const digit1 = parseInt(num / 10);
      const digit2 = parseInt(num % 10);

      const sum = digit1 + digit2;

      // const mensaje = `La suma de ${digito1} + ${digito2} = ${suma}`;

      const msg = 'La suma de ' + digit1 + ' + ' + digit2 + ' = ' + sum;
      console.log(msg);
      return sum;
    } else {
      console.log('El numero no cumple con los requisitos!');
    }

}
init= two_digits(22);
console.log(init);


module.exports = {
  two_digits,
}