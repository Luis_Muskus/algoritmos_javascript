//Leer un número entero de dos digitos y determinar si los dos digitos son iguales.
//let numero = 74;

function equal_digits(num) {
  
  if (num >= 10 && num <= 99) {
    var digit1 = parseInt(num / 10);
    var digit2 = parseInt(num % 10);
     
    if (digit1 == digit2){
      console.log(`El digito1: ${digit1} es igual a digito2: ${digit2}`);
      return true;
    }else {
      console.log('No son iguales');
    }       
  
  }else{
      console.log('No cumple con los requerimientos')
      return false;
  }
}

init= equal_digits(22);
console.log(init);
module.exports = {
  equal_digits
}