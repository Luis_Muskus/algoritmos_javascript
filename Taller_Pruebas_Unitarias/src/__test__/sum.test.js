const sum = require ( '../sum' ) ; 

test ( 'adds 1 + 2 to equal 3' , ( ) => {   
  expect ( sum ( 1 , 2 ) ) . toBe ( 3 ) ; 
} ) ;

test('dos mas dos son cuatro', () => {
    expect(2 + 2).toBe(4);
  });

  test('asignacion de objeto', () => {
    const data = {uno: 1};
    data['dos'] = 2;
    expect(data).toEqual({uno: 1, dos: 2});
  });

  test('agregando un numero positivo que no sea 0', () => {
    for (let a = 1; a < 10; a++) {
      for (let b = 1; b < 10; b++) {
        expect(a + b).not.toBe(0);
      }
    }
  });