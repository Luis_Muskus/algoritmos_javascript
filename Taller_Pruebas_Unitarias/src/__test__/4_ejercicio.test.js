const { digit_primo } = require ( '../4_ejercicio' ) ; 
//const { primo } = require ( '../4_ejercicio' ) ; 

test('Si el numero es de dos digitos retornar true', () => {

  expect(digit_primo(11)).toBeTruthy();
});
  test('Si el numero es de un digito, retorna false', () => {

    expect(digit_primo(1)).toBeFalsy();
  });

