const { final_digit } = require ( '../9_ejercicio' ) ; 
test('Si recibe flotante, retornar false', () => {

    expect(final_digit(151, 131, 52.2)).toBeFalsy();
    
  });
  test('Si el digito 2 es igual al penultimo, retornar true', () => {

    expect(final_digit(151, 131, 141)).toBeTruthy();
    
  });