const { equal_digits } = require ( '../7_ejercicio' ) ; 


test('Si el numero no es de dos digitos retornar false', () => {

  expect(equal_digits(231)).toBeFalsy();
});

  test('Si el digito 1 es igual al digito 2, retornar true', () => {

    expect(equal_digits(22)).toBeTruthy();
});