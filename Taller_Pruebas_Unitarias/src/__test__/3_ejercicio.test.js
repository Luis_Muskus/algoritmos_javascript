const { par_digits } = require ( '../3_ejercicio' ) ; 


test('Si el numero es de dos digitos retornar true', () => {

  expect(par_digits(22)).toBeTruthy();
});

test('Si los digitos son pares, retorna true', () => {

    expect(par_digits(22)).toBeTruthy();
  });
  