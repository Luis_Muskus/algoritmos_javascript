const { digit_multiplo } = require ( '../6_ejercicio' ) ; 

test('Si el numero no es de dos digitos retornar false', () => {

  expect(digit_multiplo(2)).toBeFalsy();
});

  test('Si los digitos son multiplos el uno del otro y viceversa, retornar true', () => {

    expect(digit_multiplo(24)).toBeTruthy();
});