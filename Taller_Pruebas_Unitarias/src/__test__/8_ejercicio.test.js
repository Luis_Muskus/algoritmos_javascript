const { four_digits } = require ( '../8_ejercicio' ) ; 


test('Si el numero no es de 4 digitos, retornar false', () => {

  expect(four_digits(2)).toBeFalsy();
});

test('Si el digito 2 es igual al penultimo, retornar true', () => {

    expect(four_digits (3335)).toBeTruthy();
    
  });