const { divisor } = require ( '../10_ejercicio' ) ; 
test('Si la diferencia es divisor del numero 1, retornar true', () => {

    expect(divisor(10,5)).toBeTruthy();
    
  });
  test('Si la diferencia no es divisor del numero 2, retornar false', () => {

    expect(divisor(11,5)).toBeFalsy();
    
  });