
const { two_digits } = require ( '../2_ejercicio' ) ; 

test('Si el numero es de dos digitos retornar true', () => {

  expect(two_digits(22)).toBeTruthy();
});

test('evaluar la suma de los dos digitos', () => {

  expect(two_digits(22)).toEqual(4);
});



