//Ordenar por orden alfabetico los pokemon

const pokemons = [
  'Bulbasaur',
  'Ivysaur',
  'Venomoth',
  'Diglett',
  'Dugtrio',
  'Meowth',
  'Persian',
  'Psyduck',
  'Golduck',
  'Mankey',
  'Primeape',
  'Growlithe',
  'Arcanine',
  'Poliwag',
  'Poliwhirl',
  'Poliwrath',
  'Abra',
  'Kadabra',
  'Alakazam',
  'Machop',
];
//Refactor method
const agrupados = pokemons.reduce((pokemonAgrupado, pokemon) => {
    const letra = pokemon.charAt(0);
  
    (pokemonAgrupado[letra] = pokemonAgrupado[letra] || []).push(pokemon);
    return pokemonAgrupado;
  }, {});

  /* const agrupados = pokemons.reduce((pokemonesOrdenados, pokemon) => {
  const letra = pokemon.charAt(0)
  if (pokemonesOrdenados[letra]) {
    pokemonesOrdenados[letra].push(pokemon);
  } else {
    pokemonesOrdenados[letra] = [pokemon];
  }
  return pokemonesOrdenados
}, {}) */

console.log(agrupados);

init= get_pokemon(pokemons);
console.log(init);
  
