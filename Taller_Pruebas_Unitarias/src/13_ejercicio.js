// Generar todas las tablas de multiplicar del 1 al 10
function mult(num) {
  
  for (let table = 1; table < 11; table++) {
    console.log(`\nTabla del ${table}`);
    for (let num = 0; num < 11; num++) {
      console.log(`${table} X ${num} = ${table * num}`);
    }
  }
  return true;
}

init= mult(2);
console.log(init);

module.exports = {
  mult,
}
