function isNegative(number) {
    return number < 0;
}
module.export = {
    isNegative,
};

