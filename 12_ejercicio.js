//Leer un número entero y determinar cuantas veces tiene el digito 1

function digit_repeat(num) {
    
    let n = num.toString();
    let cont=0;
    
    for (let i=1;i<=n.length;i++){
        let digit= parseInt(num % 10 );
        if(digit == 1){
            cont=cont+1;
        }
            num = num / 10;
       
        }   
    if (cont == 0) {
        console.log('no se repite')
        return false;
    }
    const msg = 'Las veces que se repite el digito 1 en el numero es ' + cont;
    console.log(msg);

}

init= digit_repeat(520);
console.log(init);

module.exports = {
    digit_repeat,
  }
