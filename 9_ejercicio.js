//Leer tres numeros enteros y determinar si el último digito. de los tres numeros es igual. 
/*num = 151;
  num= 131;
  num= 121;*/
function final_digit(num1, num2, num3){

  if(Number.isInteger(num1) == true && Number.isInteger(num2) == true && Number.isInteger(num3) == true){
    
      if (num1 >= 100 && num1 <= 999 && num2 >= 100 && num2 <= 999 && num3 >= 100 && num3 <= 999) {
        const final_digit1 = parseInt(num1 % 10);
        const final_digit2 = parseInt(num2 % 10);
        const final_digit3 = parseInt(num3 % 10);

        if (final_digit1 == final_digit2 && final_digit2 == final_digit3){
          console.log(`los ultimos digitos de ${num1} , ${num2} , ${num3} son iguales`);
          return true;
        }else{
          console.log('Los ultimos digitos no son iguales')
          return false;
        }    
      }
    }else{
    console.log('No cumple con los requesitos')
    return false;
    }
}

init= final_digit(151, 131, 121);
console.log(init);

module.exports = {
  final_digit,
}