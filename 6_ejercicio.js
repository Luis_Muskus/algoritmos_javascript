//Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.
//let numero = 74;

function digit_multiplo(num) {

    if (num >= 10 && num <= 99) {
        let digit1 = parseInt(num / 10);
        let digit2 = parseInt(num % 10);
    
        if (digit1 % digit2 == 0){
            console.log(`El digito1: ${digit1} es multiplo de digito2: ${digit2}`);
            return true;
        }else if (digit2 % digit1 == 0){
            console.log(`El digito2: ${digit2} es multiplo de digito1: ${digit1}`);
            return true;
        }else {
            console.log('No son multiplos');
            return false;
        }
    }else{
        console.log('No cumple con los requerimientos')
        return false;
    }    
    
}
init= digit_multiplo(2);
console.log(init);
module.exports = {
    digit_multiplo 
  }